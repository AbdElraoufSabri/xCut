# xCut

command line ARP poisoning tool (netcut) for Ubuntu


## Features
- [x] Protects against all netcut applications
- [x] Poison any device on the network
- [x] uses command line - no gui
- [x] Poison all devices and all except gateway

## Installation
1. Open Terminal
1. Run this command `curl https://gitlab.com/AbdElraoufSabri/xCut/raw/master/INSTALL | bash`

## Update
1. Open Terminal
1. Run this command `xx-update`

## Uninstallation
1. Open Terminal
1. Run this command `xx-uninstall`


that's it

## Tested on
- [x] Ubuntu 18.04
- [ ] Ubuntu 16.04 

## Changelog
click [here](CHANGELOG.md)
